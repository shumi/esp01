/*
 Controlling a servo position using a potentiometer (variable resistor)
 by Michal Rinott <http://people.interaction-ivrea.it/m.rinott>

 modified on 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Knob
*/

#include <ESP32Servo.h>

Servo myservo;  // create servo object to control a servo

// int potpin = 30;  // analog pin used to connect the potentiometer
// int val;    // variable to read the value from the analog pin

void setup() {
  myservo.attach(1);  // attaches the servo on pin 1 to the servo object
  Serial.begin(115200);
  while (!Serial) {
    delay(10);
  }
}

void loop() {
  // val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023)
  for(int i=0;i<180;i+=10)
  {
    // val = map(i, 0, 1023, 0, 180);     // scale it for use with the servo (value between 0 and 180)
    myservo.write(i);                  // sets the servo position according to the scaled value
    Serial.printf("sending %d\n",i);
    delay(100);                           // waits for the servo to get there
  }
}
