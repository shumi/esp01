# Dude Shemesh Repo
see docs for description. 

changelog (4.12.24):  
can send and rcv msgs, can't move the servo yet.

# description of ESP32 c6 folders
## ESP_NOW_Broadcast_Master
broadcasts "toggle" whenever rebooted.
## ESP_NOW_Broadcast_Slave
accepts first master and prints in serial the received msg (i.e. "toggle")
## Knob-servo
trying to make servo work with chip, will be easier when we soldered the pins to the breakout.

[confluence](https://shumzi.atlassian.net/wiki/spaces/KB/pages/5767437/Dude+Shemesh+Project)
